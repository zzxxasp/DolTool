package com.key.doltool.event;

import com.key.doltool.R;

public class BookEvent {
	public String[] TYPE_BASE={
			"全部","烹饪","缝纫","铸造",
			"工艺","保管","炼金","语言",
			"特别","时代"
	};
	public String[] TYPE_BASE2={
			"烹饪","缝纫","铸造",
			"工艺","保管","炼金","语言",
			"特别","时代"
	};
	public int[] TYPE_BASE_PIC={
			R.drawable.ic_recipe_1,R.drawable.ic_recipe_2,R.drawable.ic_recipe_3,
			R.drawable.ic_recipe_4,R.drawable.ic_recipe_5
	};
	public String[] TITLE={
			"全部","烹饪之道","缝纫巧思","铸造之基",
			"工艺技术","保管方法","炼金天路","语言魅力",
			"特别生产","时代配方"
	};
	public int[] PIC_ID={
			R.drawable.dol_book1,R.drawable.dol_book1,R.drawable.dol_book2,R.drawable.dol_book3,
			R.drawable.dol_book4,R.drawable.dol_book5,R.drawable.dol_book1,R.drawable.dol_book1,
			R.drawable.dol_book1,R.drawable.dol_book1
	};
}
